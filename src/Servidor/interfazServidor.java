package Servidor;



public class interfazServidor extends javax.swing.JFrame {

    /**
     * Creates new form Interface
     */
    public interfazServidor() {
        initComponents();
        addEventos();
    }
    
    public void addEventos(){
        OyenteServidor oyente = new OyenteServidor(this);
        jButton1.addActionListener(oyente);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jSeparator2 = new javax.swing.JSeparator();
        jTextField2 = new javax.swing.JTextField();
        panelServidor = new javax.swing.JPanel();
        etiquetaServidor = new javax.swing.JLabel();
        textoServidor = new javax.swing.JTextField();
        panelPuerto = new javax.swing.JPanel();
        etiquetaPuerto = new javax.swing.JLabel();
        textoPuerto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaUsuarios = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        jTextField2.setText("jTextField2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(500, 320));
        getContentPane().setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        panelServidor.setPreferredSize(new java.awt.Dimension(200, 40));

        etiquetaServidor.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        etiquetaServidor.setText("Servidor :");
        panelServidor.add(etiquetaServidor);

        textoServidor.setEditable(false);
        textoServidor.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        textoServidor.setText("localhost");
        panelServidor.add(textoServidor);

        getContentPane().add(panelServidor);

        panelPuerto.setPreferredSize(new java.awt.Dimension(200, 40));

        etiquetaPuerto.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        etiquetaPuerto.setText("Puerto : ");
        panelPuerto.add(etiquetaPuerto);

        textoPuerto.setEditable(false);
        textoPuerto.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        textoPuerto.setText("1234");
        panelPuerto.add(textoPuerto);

        getContentPane().add(panelPuerto);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(480, 182));

        tablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Importe", "Juegos", "Ganados", "Perdidos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        //tablaUsuarios.setMinimumSize(new java.awt.Dimension(75, 200));
        jScrollPane1.setViewportView(tablaUsuarios);

        getContentPane().add(jScrollPane1);

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Conectar Servidor");
        jButton1.setName("conectar"); // NOI18N
        getContentPane().add(jButton1);

        pack();
    }// </editor-fold>                        

    public static void main(String args[]) {
        interfazServidor interfaz = new interfazServidor();
        interfaz.setVisible(true);
    }

    // Variables declaration - do not modify                     
    private javax.swing.JLabel etiquetaPuerto;
    private javax.swing.JLabel etiquetaServidor;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JPanel panelPuerto;
    private javax.swing.JPanel panelServidor;
    private javax.swing.JTable tablaUsuarios;
    private javax.swing.JTextField textoPuerto;
    private javax.swing.JTextField textoServidor;
    // End of variables declaration                   

    /**
     * @return the tablaUsuarios
     */
    public javax.swing.JTable getTablaUsuarios() {
        return tablaUsuarios;
    }

    /**
     * @param tablaUsuarios the tablaUsuarios to set
     */
    public void setTablaUsuarios(javax.swing.JTable tablaUsuarios) {
        this.tablaUsuarios = tablaUsuarios;
    }
}
